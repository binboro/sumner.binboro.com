function MaterialMenu(settings) {


  this.init = function(settings) {
    this.settings = settings;

    this.openIcon = $(this.settings.openIcon);
    this.closeIcon = $(this.settings.closeIcon);
    this.menuBox = $(this.settings.menuBox);

    this.StartPositionMenu = $('.header_inner').offset().top;

    this.listenActions();

    var self = this;
    $(window).scroll(function() {

      self.scroll();
    });

    $(window).resize(function() {

      if (self.settings.actionWidth < $(window).width()) {
        $(self.menuBox).attr("style", "");
        $(self.menuBox).show();
      }
    })
  }

  this.scroll = function() {
    var top = $(window).scrollTop();
    var pos = $(this.menuBox).offset();

    if (pos.top < top) {
      $(this.menuBox).addClass("fixed_menu");
    }
    else if (this.StartPositionMenu >= top){
      $(this.menuBox).removeClass("fixed_menu");
    }
  }

  this.listenActions = function() {
    console.log("listenActions");
    var self = this;
    $(this.openIcon).click(function() {
      self.open_menu();
      $(self.openIcon).css({"opacity":0.2});
    });
    console.log(this.openIcon +" span");
    /*
    $(this.openIcon).find("span").click(function() {
      self.open_menu();
      $(self.openIcon).css({"opacity":0.2});
    });*/

    $(this.closeIcon).click(function() {
      if (self.settings.actionWidth < $(window).width())
        return false;

      $(self.closeIcon).css({"opacity":0, "transition": "all 0.5s ease"});
      $(self.menuBox).css({"transition": "all 0.9s ease"});
      self.animate_hide();
    });

    $("#material_menu_shadow").click(function() {
      if (self.settings.actionWidth < $(window).width())
        return false;
      $(self.closeIcon).css({"opacity":0, "transition": "all 0.5s ease"});
      $(self.menuBox).css({"transition": "all 0.9s ease"});
      self.animate_hide();
    });

    console.log("this.settings.parentClas",this.settings.parentClass);
    if (this.settings.parentClass) {
      var self = this;
      console.log($( this.settings.parentClass + " a"));
      $( this.settings.parentClass + ">a").click(function(e){
        if (self.settings.actionWidth < $(window).width())
          return true;

        e.preventDefault();
          var obj = $(this).parent();

          $(self.settings.childClass).hide();
          $(obj).find(self.settings.childClass).css({ position:"absolute", opacity:0, height: "auto" });
          $(obj).find(self.settings.childClass).show();

          var height = $(obj).find(self.settings.childClass).height();
          $(obj).find(self.settings.childClass).css({ height:0, transition: "all 0.7s ease"})

          setTimeout(function() {
            $(obj).find(self.settings.childClass).css({height : height, position:"relative" });
          },50);

          setTimeout(function() {
            $(obj).find(self.settings.childClass).css({ opacity:1 });
          },50);

      });
    }
  }


  this.open_menu = function() {

    $(this.menuBox).css({"right": ("-55%")});
    $(this.menuBox).show();

    this.draw_shadow();
    var self = this;
    setTimeout(function() {
      self.setup_sizes();
      self.animate_show();
    }, 50);
  }

  this.draw_shadow = function(){
    var shadow_id = "material_menu_shadow";
    $("#material_menu_shadow").remove();

    //$("body").prepend('<div id="material_menu_shadow" style="width:100%; top:0; left:0; position:absolute; z-index:100; background:#000; opacity:0.6"></div>');
    $('<div id="material_menu_shadow" style="width:100%; top:0; left:0; position:absolute; z-index:100; background:#000; opacity:0.6"></div>').insertAfter(this.menuBox);
  }

  this.setup_sizes = function() {
    var height = $(window).height();
    var bheight = $("body").height();
    var height2 = $(this.menuBox).height();

    if (height2 > height)
      height = height2;

    if (bheight > height)
      height = bheight;

    console.log("bheight: ",bheight);

    $("#material_menu_shadow").css({"height" : height});
    $(this.menuBox).css({"height": height, "z-index":901});
  }


  this.animate_show = function() {
    $(this.menuBox).css({"transition": "all 0.7s ease"});
    $(this.closeIcon).show();
    $(this.closeIcon).css({"opacity":0, "transition": "all 2.5s ease"});

    var self = this;

    setTimeout(function() {
        $(self.menuBox).css({right:0});
        $(self.closeIcon).css({opacity:1});
    }, 50);
  }


  this.animate_hide = function() {
    var self = this;

    setTimeout(function() {
      $(self.closeIcon).css({"opacity":0});
      $(self.menuBox).css({right:"-60%"});

    }, 50);


    setTimeout(function() {
      $("#material_menu_shadow").remove();
      $(self.openIcon).css({"opacity":1});
        $(self.menuBox).hide();
        $(self.closeIcon).hide();
    }, 760);
  }
}


var menu = new MaterialMenu();
$(document).ready(function() {
    var settings = {
      openIcon  : ".header_menu_open",
      closeIcon : ".header_menu_close",
      menuBox   : ".header_menu",
      parentClass : ".header_menu_li_parent",
      childClass  : ".header_menu_child",
      actionWidth : 900,
      openAll     : true,
    };
    menu.init(settings);
});