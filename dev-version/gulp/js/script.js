function Menu(){
	this.init = function(){
		var self = this;
		this.sliderHeaderInit();
		this.sliderProductsInit();
		this.sliderHeaderNav();
		this.sliderProductsNav();
		this.sliderTestimonialsInit();
		this.sliderTestimonialsNav();
		this.sliderLocationsInit();
		this.sliderLocationsNav();
		this.shadowCloser();
		this.ResizeWindow();
	}
	this.ResizeWindow = function(){
		var self = this;
		$(window).resize(function(){
			self.MenuResizer();
		});
	}
	this.MenuResizer = function(){
		var self = this;
		if($(window).width() > 900){
			$('.header_menu_close').removeAttr('style');
			$('#material_menu_shadow').remove();
		}
	}
	this.sliderHeaderInit = function(){
		var self = this;
		self.startSlider = $('.header-slider_container').owlCarousel({
			loop:true,
			nav:false,
			items: 1
		})
	}
	this.shadowCloser = function(){
		var self = this;
		var TargetElement
		$('body').click(function(e){
			TargetElement = e.target;
			if($('#material_menu_shadow').is(TargetElement)){
				$('.header_menu_close').trigger('click');
			}
		});
		$('body').bind( "touchstart", function(e){
			TargetElement = e.target;
			if($('#material_menu_shadow').is(TargetElement)){
				$('.header_menu_close').trigger('click');
				}
			} 
		);
	}
	this.sliderProductsInit = function(){
		var self = this;
		$('.our_products').find('div').last().remove();
		self.productsSlider = $('.our_products').owlCarousel({
			loop:true,
			margin:40,
			nav:false,
			// items: 5
			responsive:{
				0:{
				items:1,
				margin:0
				},
				400:{
				items:2
				},
				800:{
				items:4
				},
				900:{
				items:5,
				margin:40
				}
			}
		})
	}
	this.sliderTestimonialsInit = function(){
		var self = this;
		$('.testimonials_grid').find('div').last().remove();
		self.testimonialsSlider = $('.testimonials_grid').owlCarousel({
			loop:true,
			margin:50,
			nav:false,
			// items: 4
			responsive:{
				0:{
				items:1,
				margin:0
				},
				450:{
				items:2
				},
				800:{
				items:3
				},
				900:{
				items:4,
				margin:60
				}
			}
		})
	}
	this.sliderLocationsInit = function(){
		var self = this;
		$('.location_grid').find('div').last().remove();
		self.locationsSlider = $('.location_grid').owlCarousel({
			loop:true,
			margin:50,
			nav:false,
			// items: 4
			responsive:{
				0:{
				items:1,
				margin:0
				},
				450:{
				items:2,
				margin:20
				},
				600:{
				items:3,
				margin:20
				},
				850:{
				items:4,
				margin:60
				}
			}
		})
	}
	this.sliderHeaderNav = function(){
		var self = this;
		$('.slider-left_container').click(function() {
			self.startSlider.trigger('prev.owl.carousel');
		})
		$('.slider-right_container').click(function() {
			self.startSlider.trigger('next.owl.carousel');
		})
	}
	this.sliderProductsNav = function(){
		var self = this;
		$('.product-nav-left').click(function() {
			self.productsSlider.trigger('prev.owl.carousel');
		})
		$('.product-nav-right').click(function() {
			self.productsSlider.trigger('next.owl.carousel');
		})
	}
	this.sliderTestimonialsNav = function(){
		var self = this;
		$('.testimonials-nav-left').click(function() {
			self.testimonialsSlider.trigger('prev.owl.carousel');
		})
		$('.testimonials-nav-right').click(function() {
			self.testimonialsSlider.trigger('next.owl.carousel');
		})
	}
	this.sliderLocationsNav = function(){
		var self = this;
		$('.location-nav-left').click(function() {
			self.locationsSlider.trigger('prev.owl.carousel');
		})
		$('.location-nav-right').click(function() {
			self.locationsSlider.trigger('next.owl.carousel');
		})
	}
	this.init();
}

var Menu1 = new Menu();
$(document).ready(function() {
    Menu1;
});