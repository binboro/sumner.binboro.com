function MegaMenu() {
	this.init = function() {
    var self = this;

    this.menu_object    = $(".main-menu");
    this.body_object    = $("body");

    $(".burger-menu").click(function() {
      self.click_burger();
      // $("html, body").css("overflow","hidden");
      // $("html, body").css(style);
      $("html, body").addClass("fixed");
    });

    $(".menu-close").click(function() {
      self.close_menu();
      // $("html, body").removeAttr("style");
      $("html, body").removeClass("fixed");
    });

    $("body").click(function(e) {
      console.log("Click body: ", e);
    });

    this.myScroll = new IScroll('.main-menu',
    {   mouseWheel: true,
        scrollbars: true,
        resizeScrollbars: true,
        scrollbars: 'custom',
    });

    $(window).resize(function() {
      setTimeout(function() {
        self.myScroll.refresh();
      },510);

      if (window.innerWidth > 720) {
        self.restore_desktop();
      }
    })


	}

  this.restore_desktop = function() {
    $(this.menu_object).attr("style", "");
    this.black_cover(0);
  }

  this.click_burger = function() {
    this.open_menu();
  }

  this.enable_transition = function() {
      $(this.menu_object).css({"transition": "all 0.4s ease-in-out"});
  }

  this.disable_transition = function() {
      $(this.menu_object).css({"transition": "none"});
  }


  this.menu_start_position = function() {
    var menu = this.menu_object;
    var width =$(menu).outerWidth();
    var left = 0 - width;

    return left;
  }

  this.open_menu = function() {
    var self    = this;
    var menu    = this.menu_object;
    var body    = this.body_object;

    this.black_cover(1);

    // $(menu).show();
    // $(body).addClass("body_full-height");
		$(menu).css('display', 'table');
			$('.burger-menu').hide();
			$('.menu-close').show();
			$(body).addClass("body_full-height");
    $(menu).show();
    $(body).addClass("body_full-height");

    // Be sure start position is right
    this.disable_transition();
    var left = this.menu_start_position();
    $(menu).css({left : left + "px"});

    setTimeout(function() {
      self.enable_transition();
      $(menu).css({left : 0});
    },10);

    setTimeout(function() {
      self.myScroll.refresh()
    },500);



  }

  this.close_menu = function() {
    var self     = this;
  	var menu     = this.menu_object;
  	var body     = this.body_object;

    this.enable_transition();
    var left = this.menu_start_position();
    $(menu).css({left : left + "px"});

    $(body).removeClass("body_full-height");
		$('.burger-menu').show();
    $('.menu-close').hide();

    // setTimeout(function() {
    //   self.black_cover(0);
		//
    // },200);


    setTimeout(function() {
      self.black_cover(0);

    },200);

    setTimeout(function() {
      self.disable_transition();
    },610);

  }

  this.black_cover = function(open) {
     if (open == 1) {

      //  var style = 'style="transition:opacity 0.2s linear; opacity:0; left:0; top:0; position:absolute; z-index:8999; background:#000; width:100vw; height:100vh;"';
      //  $("body").prepend('<div class="black-cover" ' + style + '></div>');

      //  setTimeout(function() {
      //    $(".black-cover").css({"opacity":"0.6"});
      //  },10);
       var style = 'style="transition:opacity 0.2s linear; opacity:0; left:0; top:0; position:absolute; z-index:8999; background:#000; width:100vw; height:100vh;"';
       $("body").prepend('<div class="black-cover" ' + style + '></div>');

       setTimeout(function() {
         $(".black-cover").css({"opacity":"0.6"});
       },10);


     }
     else {
        // $(".black-cover").css({"opacity":"0"});

        setTimeout(function() {
          // $(".black-cover").remove();

        setTimeout(function() {
          $(".black-cover").remove();
        },300);
     }
  }
}


var mega_menu = new MegaMenu();
$(document).ready(function() {
  mega_menu.init();
});
   	mega_menu.init();
});
