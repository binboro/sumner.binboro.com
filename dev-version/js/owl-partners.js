    $(document).ready(function() {
     
      var owl = $("#owl-slideshow");
     
      owl.owlCarousel({
        autoPlay: 3000, //Set AutoPlay to 3 seconds
        items : 5,
        itemsDesktop : [1199,5],
        itemsDesktopSmall : [979,3]
     
      });
     
    });

