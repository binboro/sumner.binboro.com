function initMap() {
	var map = new google.maps.Map(document.getElementById("map_section"), {
	zoom: 13,
	scrollwheel:  false,
	center: {lat: 59.325, lng: 18.070} // Общие координаты
	});
	marker = new google.maps.Marker({
	map: map,
	draggable: true,
	position: {lat: 59.327, lng: 18.067},
	icon: "images/map/marker.png"});
}