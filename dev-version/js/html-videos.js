function Html_videos(settings) {
  this.settings = settings;
  this.id = settings.selector + "_video_" + $.now();
  this.id = this.id.split("#");
  this.id = this.id[1];

  if (this.settings.source_size){
    this.settings.source_size= this.settings.source_size.split("x");
    this.settings.source_w  = Number(this.settings.source_size[0]);
    this.settings.source_h  = Number(this.settings.source_size[1]);
  }
  else {
    this.settings.source_w = 320;
    this.settings.source_h= 240;
  }


  this.init = function() {
    var self = this;

    this.insert_video();

    $(window).resize(function() {
	     self.size();
    });

    if (this.settings.autoplay == "visible") {
      this.check_visible();
    }
  }

  this.insert_video = function() {

    this.video_container = $(this.settings.selector);

    var html ='<video muted id=' + this.id + '>';
        html += '<source src="'+this.settings.src+'" type="video/mp4">';
        html +='</div>';


    $(this.video_container).html(html);

    console.log("#" + this.id);
    this.video = $("#" + this.id);
    console.log(this.video);
    this.size();



  }

  this.size = function() {
    var width   = $(this.video_container).outerWidth();
    var height  = $(this.video_container).outerHeight();

    var s_w = this.settings.source_w;
    var s_h = this.settings.source_h;


    if (s_w > s_h) {

      var new_w = width;
      var ratio = new_w/s_w;
      var new_h = s_h * ratio;

    }
    else {
      var new_h = height;
      var ratio = new_h/s_h;
      var new_w = s_w * ratio;
    }

    if (new_h < height) {
      var ratio = height/new_h;
      new_w = new_w * ratio;
      new_h = new_h * ratio;
    }

    $(this.video).css({"width" : new_w+ "px","height" : new_h+ "px"});
    $(this.video).attr("width", new_w+"px");
    $(this.video).attr("height", new_h+"px");

    var left = width/2 - new_w/2;
    var top  = height/2 - new_h/2;
    $(this.video).css({left : left + "px", top: + top +"px", "position":"absolute"});

    this.video_width  = new_w;
    this.video_height = new_h;
  }

  this.play = function() {
    $(this.video)[0].play();
  }

  this.pause = function() {
    $(this.video)[0].pause();
  }

  this.check_visible = function() {
    var self = this;

    setTimeout(function() {

      var offset = $(self.video_container).offset();

      if (offset.left >= 0
        &&
        offset.left <= $(window).width()
        &&
        offset.top < (0 + $(window).scrollTop() + $(window).height())
        &&
        offset.top > (0 + $(window).scrollTop() - $(window).height())        
      //  &&
        //(offset.top + self.video_height) > (0 + $(window).scrollTop())

      ) {
        self.play();
      }
      else {
        self.pause();
      }

      self.check_visible();
    },1200);
  }

  this.init();
}
